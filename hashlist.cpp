#include "hashlist.hpp"

Hashlist::Hashlist(){}


Hashlist::Hashlist(string ruta):
  ruta(ruta)
{
  extraePath();
  leedirectorio();
  cout << "leídos " << v.size() << " archivos\n";
  llenar_map();
  cout << "Procesados " << m.size() << " archivos\n";
}

Hashlist::~Hashlist() {}

void Hashlist::suRuta(string ruta)
{
  this->ruta = ruta;
  this->extraePath();
  this->leedirectorio();
  this->llenar_map();
}

void Hashlist::leedirectorio()
{
  v.clear(); // Limpio vector...
  struct stat statFile;
  stat(ruta.c_str(),&statFile);
  if (S_ISREG(statFile.st_mode))
    {
      // cout << ruta << " es un archivo regular\n";
      v.push_back(ruta);
    }
  else
    {
      cout << "leo directorio: " << ruta <<  endl;
      v = read_dir(ruta);
      for (unsigned int i = 0; i < v.size();i++)
	v[i] = s_path + v[i];  // añado el path al archivo.
      // for (unsigned int i = 0; i < v.size();i++)
      // cout << v[i]<< endl;
    }
}

void Hashlist::llenar_map()
{
  m.clear(); // Limpio el contenedor.
  // cout << "lleno el registro..." << endl;
  for (unsigned int i = 0; i < v.size();i++)
    {
      entrecomillar(v[i]); // encierro entre comillas.
      v[i] = "sha1sum " + v[i];
      prepPipe(v[i].c_str(), "r");
      LeePipe(buffer,255);
      closePipe();
      quitoComillas(v[i]);
      sKey = buffer;
      v[i] = v[i].substr((v[i].find(" ") + 1),
			 v[i].length());
      m[sKey.substr(0,sKey.find(" "))] = v[i];
    }
}

void Hashlist::leeregistro()
{
  cout << "llamada a leerregistro..." << endl;
  for (map<string,string>::iterator p = m.begin();
       p != m.end(); p++)
    cout << p->first << " " << p->second << endl;
}

void Hashlist::extraePath()
{
  s_path = ruta;
  //  cout << "El path a añadir es: " << s_path << endl;
  //s_path = s_path.substr(0,s_path.rfind("/"));
  /*
  while (s_path.front() != '/')
    s_path.pop_back();
  */
  if (s_path.back() != '/')
    s_path.push_back('/');
}

bool Hashlist::is_filereg(const char * sFile,string s_path)
{
  struct stat statFile;
  string sfile = s_path + sFile; // añado ruta al archivo
  stat(sfile.c_str(),&statFile);
  if (S_ISREG(statFile.st_mode))
    return true;
  return false;
}

vector<string> Hashlist::read_dir(string directorio)
{
  vector<string> v_list;
  DIR *d_fd;
  dirent *dirent_midirectorio;
  
  if ((d_fd = opendir(directorio.c_str())))
    {
      while ((dirent_midirectorio = readdir(d_fd)))
	if ((dirent_midirectorio->d_name[0] != '.') &&
	    (is_filereg(dirent_midirectorio->d_name,s_path))) // quito archivos ocultos y compruebo si es archivo regular
	    v_list.push_back(dirent_midirectorio->d_name);
    }
  else
    {
      cerr << "error al leer directorio... " << directorio << endl;
      throw file_no_found();
      return v_list;
    }
  closedir(d_fd);
  return v_list;
}


void entrecomillar(string & cad)
{
  cad = "\"" + cad + "\"";
}

void quitoComillas(string & cad)
{
  string::iterator p;
  string s_tmp = cad;
  cad.clear();
  for (p=s_tmp.begin();p < s_tmp.end();p++)
    if (*p != '\"')
      cad.push_back(*p);
}

ofstream& operator<<(ofstream & elFlujo, Hashlist & m)
{
  for (map<string,string>::iterator p = m.begin();
       p != m.end(); p++)
    elFlujo << p->first << " " << p->second << endl;
  return elFlujo;
}
