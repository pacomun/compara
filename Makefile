CC = g++
CFLAGS = -g -Wall

%.o: %.cpp
	$(CC) -c $< $(CFLAGS)

main: prueba listado comprueba ordena_listado indexa

prueba: prueba.cpp popen.o hashlist.o
	$(CC) -o $@ $< hashlist.o popen.o  $(CFLAGS)

listado: listado.cpp popen.o hashlist.o compara.o
	$(CC) -o listado $< hashlist.o popen.o compara.o $(CFLAGS)

comprueba: comprueba.cpp popen.o hashlist.o
	$(CC) -o $@ $< hashlist.o popen.o  $(CFLAGS)

ordena_listado: ordena_listado.cpp popen.o hashlist.o
	$(CC) -o $@ $< hashlist.o popen.o  $(CFLAGS)

indexa: indexa.cpp popen.o hashlist.o compara.o
	$(CC) -o $@ $< hashlist.o popen.o compara.o $(CFLAGS)

clean:
	-rm -f *.o test listado comprueba
	-rm -f .listado_compara
	-rm -f ordena_listado
	-rm -f prueba
	-rm -f indexa


