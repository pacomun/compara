#include <fstream>
#include <map>
#include "compara.hpp"
#include "hashlist.hpp"
#include <cstring>
#include <ctime>

using namespace std;

#define ARCHIVOS_BORRADOS ".archivos_borrados"
#define	INDICE ".indice"
#define DEPOSITO "/media/pacomun/ISOIMAGE/"

int main(int argc, char ** argv)
{
  Hashlist h_file;
  string key,cad,s_fileRemove = DEPOSITO, s_index = DEPOSITO;
  s_fileRemove = s_fileRemove + ARCHIVOS_BORRADOS;
  s_index = s_index + INDICE;
  string s_dir = DEPOSITO;
  s_dir = s_dir + "modificados";
  multimap<string,string> mm;
  multimap<string,string>::iterator it;
  ifstream fin(s_index);
  ofstream fout;
  time_t t;

  // Compruebo la apertura del archivo.
  if (fin.fail())
    {
      cerr << "No hay se a encontrado el archivo "
	   << s_index << endl;
      return -1;
    }

  /* Parámetro "init" en que inicio el depósito */
  if (argc !=1 && !strcmp(argv[1],"init"))
    {
      if (mkdir(s_dir.c_str(),0744))
	{
	  cerr << "No se ha podido crear " << s_dir << endl;
	  return -1;
	}
      cout << "Creado el directorio " << s_dir << endl;
      return 0;
    }

  /* leemos archivo y cargamos cotenedor */
  while(!fin.eof())
    {
      fin >> key >> cad;
      mm.insert({key,cad});
    }
  fin.close();

  /* El bucle de lectura duplica el último elemento del contenedor. Lo
     elimino de la siguiente manera. */
  it = mm.end();
  mm.erase(--it);
  
  //  desplegarMap(mm); // depuración.

  /* Si pasamos parametro "reset" creamos una copia sí, o sí */
  if (argc != 1 && !strcmp(argv[1],"reset"))
    {
      for (auto it = mm.begin();it != mm.end();it++)
	copiaArchivo(it->second,it->first);
      return 0;
    }

  /* Si pasamos parámetro "show" muestra el contenido del indice */
  if (argc != 1 && !strcmp(argv[1],"show"))
    {
      desplegarMap(mm);
      return 0;
    }
  
  /*
    Procedo a comprobar si están actualizados los archivos de
    índice
  */
  for (auto it = mm.begin();it != mm.end(); it++)
    {
      // Pruebo a calcular el hash del archivo it->second
      try
	{
	  h_file.suRuta(it->second); // consigo el hash actual
	}
      catch (Hashlist::file_no_found)
	{
	  cerr << "¡ATENCION! el archivo  " << it->second
	       << " se ha borrado..." << endl;
	  fout.open(s_fileRemove,ios::app);
	  t = time(NULL);
	  fout << "Archivo borrado " << ctime(&t) << it->first
	       << " " << it->second << endl;
	  fout.close();
	}
      if (!h_file.empty()) // proceso si el archivo existe
	{
	  map<string,string>::iterator it_hash = h_file.begin();
	  multimap<string,string>::iterator it_find;
	  it_find = mm.find(it_hash->first);
	  if (it_find != mm.end())
	    cout << it->second << " ... OK\n";
	  else
	    {
	      copiaArchivo(it->second,it_hash->first);
	      cout << "Actualizando " << it->second << endl;
	      // registro la actualización.
	      fout.open(s_fileRemove,ios::app);
	      if (fout.fail())
		{
		  cerr << "Error al abrir para lectura el archivo "
		       << s_fileRemove << endl;
		  return -1;
		}
	      t = time(NULL);
	      fout << "Actualizado " << ctime(&t) << " "
		   << it->first << " " << it->second << endl;
	      fout.close();
	      mm.erase(it);
	      it = mm.begin(); // Vuelta al principio.
	 	      
	      mm.insert({it_hash->first,it_hash->second});
	    }
	}
      else
	{
	  mm.erase(it);
	  it = mm.begin(); 
	}
    }
  
  /* Actualizo el indice del depósito, pero antes quito elementos que
     puedan estar duplicados en el contenedor */
  
  quitarDuplicados(mm);
  fout.open(s_index);
  if (fout.fail())
    {
      cerr <<"Ocurrió un error al abrir " << s_index << endl;
      return -1;
    }
  for (auto it = mm.begin();it != mm.end(); ++it)
    {
      fout << it->first << " " << it->second << endl;
    }

  fout.close();
  
  return 0;
}
