/*
 * Lee el desde archivo creado por la utilidad listado a un contenedor
 * map y seguidamente lo compara con los archivos del directorio.

 * Devuelve 0 si la comparación tiene exito total y 1 si no conicide
 * algún archivo.
 */
#include "hashlist.hpp"
#include <fstream>

using namespace std;

int main(int argc, char** argv)
{
  Hashlist listado;
  map<string,string> archivo;
  map<string,string>::iterator p;
  map<string,string>::iterator q;
  string buffer;
  int sies = 0, noes = 0;
  
  ifstream fin(".listado_compara");
  if(fin.fail()) // por si no está el archivo.
    {
      cerr << "No se encontró el archivo .listado_compara"
	   << endl;
      return 2;
    }
    
  while (!fin.eof())
    {
      fin >> buffer;
      fin >> archivo[buffer];
    }
  
  fin.close();

  for (p = archivo.begin(); p != archivo.end(); p++)
    {
      try
	{
	  listado.suRuta(p->second);
	}
      catch (Hashlist::file_no_found)
	{
	  cout << "El archivo no se encontró...\n";
	}
	 

      q = listado.begin();
      if (p->first == q->first)
	{
	  cout << p->second << " OK." << endl;
	  sies++;
	}
      else
	{
	  cout << p->second << " NO coincide." << endl;
	  noes++;
	}
    }

  cout << "Archivos registrados: " << archivo.size() << endl;
  cout << "Total de archivos iguales: " << sies << endl;
  cout << "Total de archivo no iguales: " <<  noes << endl;

  if (noes == 0)
    return 0;
  else
    return 1;
}
