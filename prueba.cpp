#include "hashlist.hpp"

void desplegarAyuda(const char *argv);

int main(int argc, char ** argv)
{
  //  cout << "argumnetos: " << argc << endl;
  string dir1, dir2;
  int cont = 0;
  if(argc > 2)
    {
      dir1 = argv[1];
      dir2 = argv[2];
    }
  if(argc == 2)
    {
      dir1 = "./";
      dir2 = argv[1];
    }
  if (argc == 1)
    desplegarAyuda(argv[0]);

  Hashlist List_1(dir1);
  Hashlist List_2(dir2);

  map<string,string>::iterator p;
  map<string,string>::iterator f;

  //  List_1.leeregistro();
  //  List_2.leeregistro();
  
  for (p = List_1.begin(); p != List_1.end(); p++)
    {
      f = List_2.begin();
      while (f != List_2.end())
	{
	  if (p->first == f->first)
	    {
	      cout << p->second << " = " << f->second << endl;
	      cont++;
	    }
	  f++;
	}
    }

  cout << "Se encontraron: " << cont << " archivos coincidentes."
       << endl;

  cout << "Contenido de " << List_1.mostrarPath()
       << ": " << List_1.size() << " archivos procesados"
       << endl;

  cout << "Contenido de " << List_2.mostrarPath()
       << ": " << List_2.size() << " archivos procesados"
       << endl;

  return 0;
}


void desplegarAyuda(const char * argv)
{
  cout << "Uso: " << argv << " <directorio> [<directorio>]"
       << endl;
  exit(0);
}
