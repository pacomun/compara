Programa debe de llegar a  encontrar archivos de imagenes
duplicadas. Para conseguirlo utilizo la suman sha1sum, mediante una
llamda al sistema por cada archivo en el directorio actual de la
aplicación. Esta la guarda de momento en un archivo de texto plano con
sus _hash_ ordenados afabeticamente y sus respectivos nombres de
archivo. Se puede comprobar que los _hash_ coninciden con los archivos
del directorio con la orden:
	
	sha1sum -c listado.sha1sum
	
De momento queda por resolver cómo eliminar los directorios a la hora de
listar los archivos.

Resuelvo la eliminación de los directorios con la funcion `stat(2)` y
los archivos ocultos que empiezan con punto.

Dejo de grabar los datos en archivo y los cargo en un contenedor
`map`.

Comparo dos directorios con sus archivos, imprimiendo los que son iguales.

Ahora puedo comparar un archivo con un directorio, dos archivos, o dos directorioso.
