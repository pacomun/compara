
#include "compara.hpp"

using namespace std;

int ordenaListado(string listado)
{
  string buf1,buf2;
  map<string,string> m;
  std::map<string,string>::iterator it;
  ifstream arch;
  ofstream arch_of;
  arch.open(listado);
  if (arch.fail()) // por si no está el archivo.
    return 1;
  
  while (!arch.eof())
    {
      arch >> buf1 >> buf2;
      m[buf1] = buf2;
    }
  arch.close();
  
  arch_of.open(listado,std::ios_base::trunc);
  for (it = m.begin();it != m.end(); it++)
    {
      arch_of << it->first << " " << it->second << endl;
    }
  arch_of.close();
  return 0;
}

void copiaArchivo(string fuente, string destino)
{
  ifstream source(fuente, ios::binary);
  ofstream dest(destino, ios::binary);

  dest << source.rdbuf();

  source.close();
  dest.close();
}

void quitarDuplicados(multimap<string,string> & m)
{
  multimap<string,string>::iterator it,it2;
  for (it = m.begin(); it != m.end(); it++)
    {
      it2 = m.find(it->first);
 
      if (it2 != it && it->second == it2->second)
	m.erase(it2);
    }
}

void desplegarMap(multimap<string,string>&  mm)
{
  for (auto it=mm.begin();it !=mm.end(); it++)
    cout << it->first << " " << it->second << endl;
  cout << "Número de elementos: " << mm.size() << endl;
}

void rutaAbsoluta(string & s_name)
{
  string s_wd;

  /* guardo en una cadena el directorio actual de trabajo */
  s_wd = get_current_dir_name();

  /* si termina en barra la elimino directamente */
  if (s_name.back() == '/') 
    s_name.pop_back();

  /* trato el caso de que se haya especificado con la ruta absoluta y
     no necesite ninguna transformación */
  if (s_name.front() == '/')
    return;

  /* trato el caso de '../'   */
  while (!s_name.find("../")) 
    {
      while (s_wd.back() != '/')
	s_wd.pop_back();
      s_name = s_wd + s_name.substr(s_name.find_first_not_of("../"));
      cout << s_name << endl;
      return;
    }

  /* trato el caso en que referenciamos un archivo del directorio
     actual */
  if (!s_name.find("./"))
    {
      s_name = s_wd + s_name.substr(1);
      return;
    }

  /* trato el caso s_name = "./" */
  if (s_name.compare("./"))
    {
      s_name = s_wd;
      return;
    }
  
  /* por último trato el caso de que se llame al archivo
     directamente... */
  s_name = s_wd + "/" + s_name;
  return;
}
