#include "popen.h"

Pipe::Pipe()
{
}

Pipe::~Pipe()
{
}

bool Pipe::prepPipe(const char * comando, const char* modo)
{
  if ((suPipe_fp = popen(comando, modo)) == NULL)
  {
    cerr << "tubería rota" << endl;
    return false;
  }
  return true;
}

void Pipe::closePipe()
{
  pclose(suPipe_fp);
}

void Pipe::EscribePipe(char * buffer)
{
  fputs(buffer, suPipe_fp);
}

bool Pipe:: LeePipe(char *buffer, int lmax)
{
  if ((fgets(buffer, lmax, suPipe_fp)) == NULL)
    return false;
  return true;
}
