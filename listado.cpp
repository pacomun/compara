#include "hashlist.hpp"
#include "compara.hpp"
#include <fstream>
#include <cstring>


using namespace std;

#define DEPOSITO "/media/pacomun/ISOIMAGE/"
#define LISTADO ".indice"

int main(int argc, char** argv)
{
  std::ios_base::openmode modo;
  string s_name, s_index = DEPOSITO;
  s_index = s_index + LISTADO;
  Hashlist listado;

  if (argc <= 2 )
    {
      cerr << "Uso: \n"
	   << argv[0] << " "
	   << "<acción> <archivo | directorio>\n";
      return 1;
    }

  /* Debe de haber un archivo en argv[2] */
  s_name = argv[2];
  rutaAbsoluta(s_name);

  // cout << "rutaAbsoluta: " << s_name << endl;

  /* contemplo varias opciones de apertura del archivo */
  if (!strcmp(argv[1],"add"))
    {
      modo = std::ios_base::app;
    }
  else
    if (!strcmp(argv[1],"create"))
      {
	modo = std::ios_base::trunc;
      }
    else
      {
	cerr << "acciones contempladas: [ add | create ]\n";
	return 1;
      }

  /* creo un objeto Hashlist con el archivo */
  try
    {
      listado.suRuta(s_name);
    }
  catch (Hashlist::file_no_found)
    {
      cerr << "El archivo " << argv[2] << " no se encuentra.\n";
      return -1;
    }

  /* escribo en resultado en el archivo según el modo elegido en la
     opciones anteriores */
  ofstream out(s_index, modo);
  if (out.fail())
    {
      cerr << "Error al abrir " << s_index << endl;
      return -1;
    }
  cout << s_index << endl;
  out << listado;
  out.close();

  /* antes de retornar ordeno el archivo y quito duplicados */
  return ordenaListado(s_index);
}
