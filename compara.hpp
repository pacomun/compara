#include "hashlist.hpp"


using namespace std;

int ordenaListado(string listado);
/* Lee y reescribe un archivo tipo listado para ordenarlo. */

void copiaArchivo(string fuente, string destino);
/* copia archivo fuente en destino */

void quitarDuplicados(multimap<string,string> & m);
/* quitar los elementos duplicados del multimap */

void desplegarMap(multimap<string,string>& m);
/* función para motrar el contenido del contenedor; para facilitar
   tareas de depuración */

void rutaAbsoluta(string & s_name);
/* Pasamos una ruta de archivo y se cambia a una ruta absoluta */
