//  Definición del objeto Pipe encargado de la lectura
//  y escritura en una tubería.

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

#ifndef C_pipe_H
#define C_pipe_H

class Pipe
{
  public:
    Pipe();
    ~Pipe();
    bool prepPipe(const char* comando, const char *modo);
    void closePipe();
    void EscribePipe(char *);
    bool LeePipe(char *, int);

  protected:
    FILE *suPipe_fp;
};
#endif
