// Definición de la clase Hashlist que deriva de Pipe.

#ifndef H_HASHLIST_H
#define H_HASHLIST_H
#include "popen.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <dirent.h>
#include <map>
#include <string>
#include <vector>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>



using namespace std;

class Hashlist : public Pipe
{
public:
  Hashlist();
  Hashlist(string); // Constructor al que pasamos el archivo.
  ~Hashlist();
  void suRuta(string); // Incializa una ruta.
  void leeregistro(); // Desplega los registros procesados.
  const  map<string,string>::iterator begin()
  { return m.begin(); }
  const  map<string,string>::iterator end()
  { return m.end(); }
  unsigned int size() { return m.size(); } // Devuelve archivos procesados.
  string mostrarPath()
  { return ruta; }
  friend ofstream& operator<<(ofstream &  elFlujo, Hashlist & m);
  // encuentra en su map
  map<string,string>::iterator find(const string & key)
  { return m.find(key); }
  class file_no_found {};  // Excepción
  bool empty() { return m.empty(); }
  
protected:
  void leedirectorio();
  void llenar_map();
  void extraePath();
  bool is_filereg(const char *,string);
  map<string,string> m;
  vector<string> read_dir(string directorio);
  string ruta, s_path;
  vector<string> v;
  string sKey;
  char buffer[255];
};

//Funciones no miembros

void entrecomillar(string &);
void quitoComillas(string &);
#endif
