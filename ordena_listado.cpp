#include "hashlist.hpp"
#include <fstream>
#include <string>


using namespace std;

int main(int argc,char** argv)
{
  string buf1,buf2;
  map<string,string> m;
  std::map<string,string>::iterator it;
  ifstream arch;
  ofstream arch_of;
  arch.open(".listado_compara");

  while (!arch.eof())
    {
      arch >> buf1 >> buf2;
      m[buf1] = buf2;
    }
  arch.close();
  
  cout << "\nSalida del map: " << m.size() << endl;
  arch_of.open(".listado_compara");
  for (it = m.begin();it != m.end(); it++)
    {
      cout << it->first << " " << it->second << endl;
      arch_of << it->first << " " << it->second << endl;
    }
  arch_of.close();
  return 0;
}
